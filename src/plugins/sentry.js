import Vue from 'vue';
import * as Sentry from '@sentry/browser';
import { Vue as VueIntegration } from '@sentry/integrations';

Sentry.init({
  dsn: 'https://bdf30c5ef7904e4bb461da5e68a163cd@o146167.ingest.sentry.io/5220110',
  integrations: [new VueIntegration({ Vue, attachProps: true })],
});
